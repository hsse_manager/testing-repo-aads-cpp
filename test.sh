#! /usr/bin/env bash

test -s test_file
if [[ $1 == "master" ]]
then 
  echo "No testing on master branch"
elif [[ $1 == "main" ]]
then
    echo "No testing on main branch"
else 
  echo "begin testing $1"
  cp testing_repo/clang-tidy.json $1/clang-tidy.json
  cd $1
  if [[ ! $? -eq 0 ]]
  then 
    echo "А где папка с именем ветки?"
    exit 1
  fi
  touch .cpp

  echo "clang-tidy check"
  clang-tidy --config-file=clang-tidy.json  -extra-arg=-std=c++20 -quiet main.cpp
  if [[ ! $? -eq 0 ]]
  then 
    echo "Эх, на первом же тесте :("
    exit 1
  fi
  echo "clang-tidy achieved"

  echo "clang-format check"
  clang-format -i -style='{BasedOnStyle: Google, DerivePointerAlignment: false, PointerAlignment: Left}' main.cpp &&
      git diff --ignore-submodules --color > diff &&
      cat diff
  if [[ -s diff ]]
  then 
    echo "Ну как там с форматом?"
    exit 1
  fi
  echo "clang-format achieved"

  echo "g++ with -Wall -Werror -Wextra -pedantic check"
  g++ -Wall -Werror -Wextra -pedantic -std=c++20 main.cpp -o main.out
  if [[ ! -s main.out ]]
  then
    echo "Пранк флагами не пройден :("
    exit 1
  fi

  rm main.out

  echo "clang++-18 with -Wall -Werror -fsanitize=address,undefined check"
  clang++-13 -std=c++20 -Werror  -pedantic -Wall -Wextra -fsanitize=address,undefined main.cpp -o main.out 

  if [[ ! -s main.out ]]
  then
    echo "Пранк clang'ом не пройден :("
    exit 1
  fi


  echo "Они прошли четыре стадии? Выпускайте valgrind!"

  if [[ ! -s ../testing_repo/$1.txt ]]
  then 
    echo "А его нет :("
  else 
    valgrind --leak-check=yes --log-file=log.txt ./main.out < ../testing_repo/$1.txt
    echo "Valgrind log:"
    cat log.txt
    python3 ../testing_repo/valgrind_parser.py
    if [[ $? -eq 1 ]]
    then 
      echo "А кто украл free?"
      exit 1
    fi
  fi
  echo "Вы потрясающие!"
fi
